#Blender-Low-Polly-Game-Default-Scene

Copyright Kris Occhipinti 2021-09-19

(https://filmsbykris.com)

License GPLv3

This File was created by Kris Occhipinti

https://filmsbykris.com


based on a tutorial by Imphenzia

(https://www.youtube.com/watch?v=BlxiCd0Upg4)


Texture palette also by Imphenzia

You can get it here

(https://www.dropbox.com/s/c5olic38j8fopet/ImphenziaPalette01.png?dl=0)

although it should be embeded in this blend file
